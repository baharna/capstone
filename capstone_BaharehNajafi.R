# Applying multi-regression
chronic_disease <- as.data.frame(read.csv(file="/Users/Hadish/Downloads/ccdss-scsmc-eng.csv", header = TRUE, sep = ","))
#white_IHD <- filter(white_wine, Disease = IHD)
white_IHD = chronic_disease[2354:2689,]
white_IHD2 <- white_IHD[ -c(10:15) ]
white_IHD2$Gender<-as.factor(white_IHD2$Gender)
white_IHD2$Age.Group<-as.factor(white_IHD2$Age.Group)
white_IHD3 <- subset(white_IHD2, white_IHD2$Gender=='M')
white_IHD4 <-subset(white_IHD3, white_IHD3$Age.Group == '50 to 54' | white_IHD3$Age.Group == '55 to 59' 
                    | white_IHD3$Age.Group == '60 to 64' | white_IHD3$Age.Group == '65 to 69' | white_IHD3$Age.Group == '70 to 74' | white_IHD3$Age.Group == '75 to 79' | white_IHD3$Age.Group == '80 to 84')
white_IHD5 <- white_IHD4[ -c(1:2) ]
white_IHD6 <- white_IHD5[ -c(1:2, 4, 7, 10:15, 18:19) ]
options("scipen"=100, "digits"=4)
all_var <-lm(Mortality.with.Disease ~ . , data=white_IHD6)
aaa_var <- lm(aaa[,2] ~ aaa[,c(1, 3:4)])
white_IHD8$Mortality.with.Disease[which(white_IHD8$Mortality.with.Disease == NA)] = 1
aaa = matrix(as.numeric(unlist(white_IHD6)),nrow=nrow(white_IHD6))
all_var <- lm(aaa[,2] ~ aaa[,c(1, 3:7)])
best_var <- stepAIC(all_var, direction= "backward", trace=TRUE)
bb3 = as.data.frame(aaa)
best_fit = regsubsets(V3 ~ . , data=bb3, nbest=1, method = "backward")
summary_best_fit = summary(best_fit)
summary_best_fit
summary_best_fit$rss
summary_best_fit$rsq