import numpy as np
csv = np.zeros((336, 27), dtype = object)
csv = np.genfromtxt ('ccdss-scsmc-eng.csv', delimiter=",")
csv_HDI = csv[2354:2690,1:27]
csv_HDI2 = np.concatenate((csv_HDI[:,0:8], csv_HDI[:, 14:26]), axis = 1)
age_range = np.arange(1, 15)
# converting categorical variable to numeric
for i in np.arange(1, np.true_divide(np.shape(csv_HDI2)[0], np.shape(age_range)[0]) + 1):
    csv_HDI2[(np.size(age_range)) * (i-1):np.size(age_range) * i, 2] = age_range 

for i in np.arange(1, np.true_divide(np.shape(csv_HDI2)[0], np.shape(age_range)[0]) + 1):
    if i - np.floor(np.true_divide(i, 2)) * 2 == 0:
       csv_HDI2[(np.size(age_range)) * (i-1):np.size(age_range) * i, 1] = 1
    else:
        csv_HDI2[(np.size(age_range)) * (i-1):np.size(age_range) * i, 1] = 0

# Plotting prevalence/mortality cases versus gender and age group
csv_plot = np.zeros((12, 2 * np.size(age_range)))
csv_plot_ageS = np.zeros((28, 2))
for j in np.arange(12):
    csv_plot_ageS = csv_HDI2[(2 * np.size(age_range)) * j:(2 * np.size(age_range)) * (j + 1),1:3]
    for k in np.arange(2 * np.size(age_range)):
        if csv_plot_ageS[k,0] == 0:
           csv_plot[j, (csv_plot_ageS[k,1]-1) * 2] = csv_HDI2[2 * np.size(age_range) * j + k,16]
        else:
            csv_plot[j, csv_plot_ageS[k,1] * 2 - 1] = csv_HDI2[2 * np.size(age_range) * j + k,16]
    
prevalent_genderFVSN = np.concatenate((csv_plot[:,0], csv_plot[:,2], csv_plot[:,4], csv_plot[:,6], csv_plot[:,8], csv_plot[:,10], csv_plot[:,12], csv_plot[:,14], csv_plot[:,16], csv_plot[:,18], csv_plot[:,20], csv_plot[:,22], csv_plot[:,24], csv_plot[:,26]), axis = 0)
years = np.arange(1999, 2011)

prevalent_genderMVSN = np.concatenate((csv_plot[:,1], csv_plot[:,3], csv_plot[:,5], csv_plot[:,7], csv_plot[:,9], csv_plot[:,11], csv_plot[:,13], csv_plot[:,15], csv_plot[:,17], csv_plot[:,19], csv_plot[:,21], csv_plot[:,23], csv_plot[:,25], csv_plot[:,27]), axis = 0)
# prevalent_agegroup = np.concatenate((csv_plot(:,1], csv_plot(:,3])
import matplotlib.pyplot as plt

plt.plot([0, 1], [sum(prevalent_genderF)/12, sum(prevalent_genderM)/12])
plt.ylabel('prevalent cases versus gender')
plt.show()

age+p = np.zeros((1, 14))
csv_plot_meandeP = np.zeros((1, np.shape(csv_plot)[1]))
for k in np.arange(np.shape(csv_plot)[1]):
    csv_plot_meandeP[0, k] = np.mean(csv_plot[:, k])

csv_plot_meanFdeP= np.zeros((1, np.size(age_range)))
csv_plot_meanMdeP= np.zeros((1, np.size(age_range)))
for k in np.arange(np.shape(csv_plot_meandeP)[1]):
    if k - np.floor(np.true_divide(k, 2)) * 2 == 0:
       csv_plot_meanFdeP[0, np.true_divide(k, 2)] = csv_plot_meandeP[0, k]
    else:
       csv_plot_meanMdeP[0, np.true_divide(k-1, 2)] = csv_plot_meandeP[0, k] 

n_groups = 14

# means_men = (20, 35, 30, 35, 27)
# std_men = (2, 3, 4, 1, 2)

# means_women = (25, 32, 34, 20, 25)
# std_women = (3, 5, 2, 3, 3)

fig, ax = plt.subplots()

index = np.arange(n_groups)
bar_width = 0.35

opacity = 0.4
error_config = {'ecolor': '0.3'}

opacity = 0.4
error_config = {'ecolor': '0.3'}

rects1 = plt.bar(index, means_men, bar_width,
                 alpha=opacity,
                 color='b',
                 error_kw=error_config,
                 label='Men')
rects2 = plt.bar(index + bar_width, means_women, bar_width,
                 alpha=opacity,
                 color='r',
                 error_kw=error_config,
                 label='Women')
plt.xlabel('Age_Group')
plt.ylabel('Mortality_cases(%)')
plt.title('Mortality cases/population by age group and gender')
plt.xticks(index + bar_width, (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14))
plt.legend()

plt.tight_layout()
plt.show()
import math

def average(x):
    assert len(x) > 0
    return float(sum(x)) / len(x)

def pearson_def(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff
    return diffprod / math.sqrt(xdiff2 * ydiff2)


# PCA Analysis for feature selection
def test_PCA(data, dims_rescaled_data=2):
    '''
    test by attempting to recover original data array from
    the eigenvectors of its covariance matrix & comparing that
    'recovered' array with the original data
    '''
    _ , _ , eigenvectors = PCA(data, dim_rescaled_data=2)
    data_recovered = NP.dot(eigenvectors, m).T
    data_recovered += data_recovered.mean(axis=0)
    assert NP.allclose(data, data_recovered)

from pydoc import help
from scipy.stats.stats import pearsonr
pearsonr(prevalent_genderFde, prevalent_genderF)

# Applying K-means clustering algorithm (k=2)
#for having 2 regime
K = np.zeros((168, 1))
for j in np.arange(np.size(age_range) + 1):
    K[np.size(years) * (j-1):np.size(years) *j,0] = j

W = np.zeros((168, 3))  
W[:,0] = K[:,0]
W[:,1] = prevalent_genderM
W[:,2] = prevalent_genderMde
W1 = np.zeros((1, np.shape(C1_new)[0]))
W2 = np.zeros((1, np.shape(C2_new)[0]))
# W = np.concatenate((K[0], prevalent_genderF, prevalent_genderFde), axis = 1)
for j in np.arange(np.shape(C1_new)[0]):
    W1[0, j] = W[np.where(W[:,1:3] == C1_new[j,:])[0][0], 0]
    
for j in np.arange(np.shape(C2_new)[0]):
    W2[0, j] = W[np.where(W[:,1:3] == C2_new[j,:])[0][0], 0]
    
W3_new = np.zeros((168,2)) 
W3_new[:,0] = prevalent_genderM
W3_new[:,1] = prevalent_genderMde 
prevalent_genderMde[157:162] = np.mean(prevalent_genderMde[np.nonzero(prevalent_genderMde[156:168])])
     
# W3_new=np.reshape(W3[np.nonzero(W3)], (W3[np.nonzero(W3)].shape[0]/2,2))
U=np.zeros((W3_new.shape[0],2))
C1=np.zeros((W3_new.shape[0],2))
C2=np.zeros((W3_new.shape[0],2))
V=np.zeros((1,200))

row, col = W3_new.shape[0], 2
matrix = np.random.randint(2, size=(row, col))
U[:,0]=matrix[:,0]
a=np.arange(W3_new.shape[0])
for x in np.nditer(a):
    if U[x,0] == 0:
       U[x,1]=1
    else:
       continue

k=1;
for x in np.nditer(a):
    if U[x,0] == 1:
       C1[k-1,0]=W3_new[x,0]
       C1[k-1,1]=W3_new[x,1]
    else:
       C2[k-1,0]=W3_new[x,0]
       C2[k-1,1]=W3_new[x,1]
    k+=1;

C1_new=np.reshape(C1[np.nonzero(C1)], (C1[np.nonzero(C1)].shape[0]/2,2))
C2_new=np.reshape(C2[np.nonzero(C2)], (C2[np.nonzero(C2)].shape[0]/2,2))
V[0,0:2]=np.mean(C1_new, axis=0)
V[0,2:4]=np.mean(C2_new, axis=0)
p=1;
e=20;
while e > 0.05:
    D1=np.zeros(np.shape(C1_new))
    D2=np.zeros(np.shape(C2_new))
    C1_new_exc=np.zeros(np.shape(C1_new))
    C2_new_exc=np.zeros(np.shape(C2_new))
    n=1; 
    for m in np.nditer(np.arange(np.shape(D1)[0])):
        D1[m,0]=((C1_new[m,0]-V[0,4*(p-1)]) ** 2 + (C1_new[m,1]-V[0,4*(p-1)+1]
        ) ** 2) ** 0.5  
        D1[m,1]=((C1_new[m,0]-V[0,4*(p-1)+2]) ** 2 + (C1_new[m,1]-V[0,4*(p-1)+3]
        ) ** 2) ** 0.5
        if D1[m,0] > D1[m,1]:
           C1_new_exc[n-1,:]=C1_new[m,:]
           C1_new[m,:]=0
           n+=1;
        else:
           continue 
    C1_new=np.reshape(C1_new[np.nonzero(C1_new)],(C1_new[np.nonzero(C1_new)].
    shape[0]/2,2))
    
    C1_new_exc=np.reshape(C1_new_exc[np.nonzero(C1_new_exc)], (C1_new_exc[np.
    nonzero(C1_new_exc)].shape[0]/2,2))
    l=1;
    for q in np.nditer(np.arange(np.shape(D2)[0])):
        D2[q,0]=((C2_new[q,0]-V[0,4*(p-1)]) ** 2 + (C2_new[q,1]-V[0,4*(p-1)+1]
        ) ** 2) ** 0.5  
        D2[q,1]=((C2_new[q,0]-V[0,4*(p-1)+2]) ** 2 + (C2_new[q,1]-V[0,4*(p-1)+3]
        ) ** 2) ** 0.5
        if D2[q,0] < D2[q,1]:
           C2_new_exc[l-1,:]=C2_new[q,:]
           C2_new[q,:]=0
           l+=1;
        else: 
           continue
    C2_new=np.reshape(C2_new[np.nonzero(C2_new)], (C2_new[np.nonzero(C2_new)].
    shape[0]/2,2))
    C2_new_exc=np.reshape(C2_new_exc[np.nonzero(C2_new_exc)], (C2_new_exc[np.
    nonzero(C2_new_exc)].shape[0]/2,2))
    l=1;
    C1_new=np.concatenate((C1_new,C2_new_exc), axis=0)
    C2_new=np.concatenate((C2_new,C1_new_exc), axis=0)
    V[0,4*p:4*p+2]=np.mean(C1_new, axis=0)
    V[0,4*p+2:4*p+4]=np.mean(C2_new, axis=0)  
    e_prep=np.power(np.subtract(V[0,4*p-4:4*p], V[0,4*p:4*p+4]), 2)
    e=(e_prep[0] + e_prep[1]) ** 0.5 + (e_prep[2] + e_prep[3]) ** 0.5
    p+=1;
    print (p,"Counter", e, "Error")

C1_new_percernt=np.true_divide(C1_new.shape[0], W3_new.shape[0])
C2_new_percernt=np.true_divide(C2_new.shape[0], W3_new.shape[0])
print (C1_new_percernt,"Cluster 1 Percent(%)")
print (C1_new[:,1].min(), "min density of Cluster 1", C1_new[:,1].max(), "max of cluster 1")
print (C1_new[:,0].mean(), "mean speed of cluster 1")
print (C2_new_percernt,"Cluster 2 Percent(%)")
print (C2_new[:,1].min(), "min density of Cluster 2", C2_new[:,1].max(), "max of cluster 2")
